package es.deusto.onthestreet;

import org.junit.Test;

import static org.junit.Assert.*;

/*
 *    Autor: Julen San Tirso Hernández
 *    Nombre de archivo : ExampleUniTest.java
**/

public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }
}