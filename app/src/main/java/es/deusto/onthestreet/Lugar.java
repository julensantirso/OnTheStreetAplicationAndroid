package es.deusto.onthestreet;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

/*
 *    Autor: Julen San Tirso Hernández
 *    Nombre de archivo : Lugar.java
**/

public class Lugar implements Serializable{
    private String Nombre;
    private double Lat;
    private double Lon;
    private String Descripcion;
    private ArrayList<String> ListaContactos;

    public Lugar(String nombre, double lat, double lon, String descripcion) {
        Nombre = nombre;
        Lat = lat;
        Lon = lon;
        Descripcion = descripcion;
        ListaContactos = new ArrayList<String>();    }

    public Lugar() {
        ListaContactos = new ArrayList<String>();
    }

    public String getNombre() {
        return Nombre;
    }

    public ArrayList<String> getListaContactos() {
        return ListaContactos;
    }

    public void setLat(double lat) {
        Lat = lat;
    }

    public void setLon(double lon) {
        Lon = lon;
    }

    public void setListaContactos(ArrayList<String> listaContactos) {
        ListaContactos = listaContactos;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public double getLat() {
        return Lat;
    }

    public double getLon() {
        return Lon;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

    public void add(String name) {
        this.ListaContactos.add(name);
    }

    @Override
    public String toString() {
        return "Lugar{" +
                "Nombre='" + Nombre + '\'' +
                ", Lat=" + Lat +
                ", Lon=" + Lon +
                ", Descripcion='" + Descripcion + '\'' +
                ", ListaContactos=" + Arrays.asList(ListaContactos) +
                '}';
    }
}
