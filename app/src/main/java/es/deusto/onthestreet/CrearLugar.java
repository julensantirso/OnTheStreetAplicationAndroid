
package es.deusto.onthestreet;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

/*
 *    Autor: Julen San Tirso Hernández
 *    Nombre de archivo : CrearLugar.java
**/


public class CrearLugar extends AppCompatActivity {

    private static final int SELECT_CONTACT = 0;
    private Button guardar;
    private Button cancelar;
    private Button agregar;
    private EditText lugar;
    private EditText longitud;
    private EditText latitud;
    private EditText descripcion;
    private Lugar l;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_lugar);

        cancelar = (Button) findViewById(R.id.buttonCancelar);
        guardar = (Button) findViewById(R.id.buttonGuardar);
        agregar = (Button) findViewById(R.id.buttonAgregar);
        lugar = (EditText) findViewById(R.id.editTextLugar);
        latitud = (EditText) findViewById(R.id.editTextLatitud);
        longitud = (EditText) findViewById(R.id.editTextLongitud);
        descripcion = (EditText) findViewById(R.id.editTextDescripcion);

        l = new Lugar();


        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("guardar", latitud.getText().toString());
                Log.d("guardar", longitud.getText().toString());
                l.setNombre(lugar.getText().toString());
                l.setLat(Double.parseDouble(latitud.getText().toString()));
                l.setLon(Double.parseDouble(longitud.getText().toString()));
                l.setDescripcion(descripcion.getText().toString());
                try {
                    guardar(getActivity(), l);
                    Log.d("Guardar", l.toString());
                    Log.d("Guardar", "guardado ok");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        agregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
                startActivityForResult(intent, SELECT_CONTACT);
            }
        });

    }

    @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SELECT_CONTACT) {
            if (resultCode == RESULT_OK) {
                Cursor cursor = getContentResolver().query(data.getData(), new String[] {
                        ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone.NUMBER}, null, null, null);
                if(cursor.moveToNext()){
                    Log.i("Intent:",data.getDataString());
                    l.add(cursor.getString(0));
                    Log.d("Lugar", "Numero de contactos: "+l.getListaContactos().size());
                    return; }
            }
        }
    } // Recommended super.onActivityResult(requestCode, resultCode, data);
        // }

    private Context getActivity() {
        return this;
    }

    protected  void guardar(Context context, Object o) throws IOException {
        File file = new File(context.getFilesDir(), "lugar.data");
        ObjectOutputStream objectOutputStream =
                new ObjectOutputStream(new FileOutputStream(file));
        objectOutputStream.writeObject(o);
        objectOutputStream.close();
    }


}
