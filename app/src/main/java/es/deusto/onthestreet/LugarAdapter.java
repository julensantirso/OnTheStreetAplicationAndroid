package es.deusto.onthestreet;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/*
 *    Autor: Julen San Tirso Hernández
 *    Nombre de archivo : LugarAdapter.java
**/


public class LugarAdapter extends ArrayAdapter<Lugar> {

    Context context;
    int layoutResourceId;
    ArrayList<Lugar> data;

    public LugarAdapter(Context context, int layoutResourceId, ArrayList data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        LugarHolder holder = null;

        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new LugarHolder();
            holder.imgIcon = (TextView)row.findViewById(R.id.textViewLatLong);
            holder.txtTitle = (TextView)row.findViewById(R.id.textViewItemTitle);

            row.setTag(holder);
        }
        else
        {
            holder = (LugarHolder)row.getTag();
        }

        Lugar weather = data.get(position);
        holder.txtTitle.setText(weather.getNombre());
        holder.imgIcon.setText(weather.getLat()+","+ weather.getLon());

        return row;
    }

    static class LugarHolder
    {
        TextView imgIcon;
        TextView txtTitle;
    }
}