package es.deusto.onthestreet;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

/*
 *    Autor: Julen San Tirso Hernández
 *    Nombre de archivo : OnTheStreetActivity.java
**/

public class OnTheStreetActivity extends AppCompatActivity {

    private ArrayList<Lugar> listalugares;
    private ListView listviewLugares;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_on_the_street);
        
        listviewLugares = (ListView) findViewById(R.id.listviewLugares);
        
        listalugares = new ArrayList<>();
        for (int i=0; i <3; i++){
            listalugares.add(new Lugar("Nombre"+i,1.0d+i, 2.0d+i, "Descripcion"+i));
        }

        LugarAdapter adapter = new LugarAdapter(this,R.layout.item_lista_lugar, listalugares);
        listviewLugares.setAdapter(adapter);

        listviewLugares.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                // ListView Clicked item index
                int itemPosition     = position;

                // ListView Clicked item value
                Lugar  itemValue    = (Lugar) listviewLugares.getItemAtPosition(position);

                // Show Alert
                Toast.makeText(getApplicationContext(),
                        "Position :"+itemPosition+"  ListItem : " +itemValue.getNombre() , Toast.LENGTH_LONG)
                        .show();

            }

        });
    }
    @Override

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.adlugar:
                adlugar();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void adlugar() {
        Intent t =new Intent(this, CrearLugar.class);
        startActivity(t);
    }


}
