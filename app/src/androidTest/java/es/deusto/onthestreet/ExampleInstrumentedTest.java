package es.deusto.onthestreet;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

/*
 *    Autor: Julen San Tirso Hernández
 *    Nombre de archivo : ExampleInstrumentedTest.java
**/

@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("es.deusto.onthestreet", appContext.getPackageName());
    }
}
